<?php

use Adianti\Control\TAction;
use Adianti\Control\TPage;
use Adianti\Registry\TSession;
use Adianti\Widget\Container\TPanelGroup;
use Adianti\Widget\Container\TVBox;
use Adianti\Widget\Datagrid\TDataGrid;
use Adianti\Widget\Datagrid\TDataGridAction;
use Adianti\Widget\Datagrid\TDataGridColumn;
use Adianti\Widget\Datagrid\TPageNavigation;
use Adianti\Widget\Form\TCombo;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TSelect;
use Adianti\Widget\Util\TXMLBreadCrumb;
use Adianti\Wrapper\BootstrapDatagridWrapper;
use Adianti\Wrapper\BootstrapFormBuilder;

class PessoasList extends TPage
{
    protected $form;
    protected $datagrid;
    protected $pageNavigation;

    use Adianti\Base\AdiantiStandardListTrait;

    /**
     * Class constructor
     * Creates the page, the form and the listing
     */
    public function __construct()
    {
        parent::__construct();

        $this->setDatabase('treinamento');
        $this->setActiveRecord('Pessoa');
        $this->addFilterField('nome', 'like', 'nome');
        $this->addFilterField('cpf', 'like', 'cpf');
        $this->addFilterField('sexo', '=', 'sexo');
        $this->setDefaultOrder('id', 'asc');

        // creates the form
        $this->form = new BootstrapFormBuilder('form_search_pessoas');
        $this->form->setFormTitle("Listagem de Pessoas");

        $nome = new TEntry('nome');
        $cpf = new TEntry('cpf');
        $cpf->setMask('999.999.999-99', true);

        $sexo     = new TCombo('sexo');
        $opcoesSexo = ["M" => 'Masculino', "F" => "Feminino"];
        $sexo->addItems($opcoesSexo);

        $this->form->addFields([new TLabel('Nome:')], [$nome]);
        $this->form->addFields([new TLabel('CPF:')], [$cpf]);
        $this->form->addFields([new TLabel('Sexo:')], [$sexo]);

        // add form actions
        $this->form->addAction('Buscar', new TAction([$this, 'onSearch']), 'fa:search blue');
        $this->form->addActionLink('Novo',  new TAction(['PessoasForm', 'onClear']), 'fa:plus-circle green');
        $this->form->addActionLink('Limpar',  new TAction([$this, 'clear']), 'fa:eraser red');

        // keep the form filled with the search data
        $this->form->setData(TSession::getValue('PessoasList_filter_data'));

        // creates the DataGrid
        $this->datagrid = new BootstrapDatagridWrapper(new TDataGrid);
        $this->datagrid->width = "100%";

        // creates the datagrid columns
        $col_id    = new TDataGridColumn('id', 'Id', 'right', '10%');
        $col_nome  = new TDataGridColumn('nome', 'Nome', 'left', '60%');
        $col_cpf = new TDataGridColumn('cpf', 'CPF', 'center', '30%');
        $col_sexo = new TDataGridColumn('sexo', 'Sexo', 'center', '30%');
        $col_cidade = new TDataGridColumn('cidade->nome', 'Cidade', 'center', '30%');

        $col_sexo->setTransformer(function ($value, $object) {
            if ($value == 'M')
                return "Masculino";
            return "Feminino";
        });

        $this->datagrid->addColumn($col_id);
        $this->datagrid->addColumn($col_nome);
        $this->datagrid->addColumn($col_cpf);
        $this->datagrid->addColumn($col_sexo);
        $this->datagrid->addColumn($col_cidade);

        $col_id->setAction(new TAction([$this, 'onReload']),   ['order' => 'id']);
        $col_nome->setAction(new TAction([$this, 'onReload']), ['order' => 'nome']);

        $action1 = new TDataGridAction(['PessoasForm', 'onEdit'],   ['key' => '{id}']);
        $action2 = new TDataGridAction([$this, 'onDelete'],   ['key' => '{id}']);

        $this->datagrid->addAction($action1, 'Edit',   'far:edit blue');
        $this->datagrid->addAction($action2, 'Delete', 'far:trash-alt red');

        // create the datagrid model
        $this->datagrid->createModel();

        // creates the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));

        // creates the page structure using a table
        $vbox = new TVBox;
        $vbox->style = 'width: 100%';
        $vbox->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $vbox->add($this->form);
        $vbox->add(TPanelGroup::pack('', $this->datagrid, $this->pageNavigation));

        // add the table inside the page
        parent::add($vbox);
    }

    /**
     * Clear filters
     */
    function clear()
    {
        $this->clearFilters();
        $this->onReload();
    }
}
