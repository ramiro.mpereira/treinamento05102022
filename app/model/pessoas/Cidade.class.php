<?php

use Adianti\Database\TRecord;

class Cidade extends TRecord
{
    const TABLENAME = 'cidades';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
    

    /**
     * Constructor method
     */
    public function __construct($id = NULL)
    {
        parent::__construct($id);
        parent::addAttribute('nome');
        parent::addAttribute('uf');
    }

    
}
