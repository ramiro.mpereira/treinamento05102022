<?php

use Adianti\Database\TRecord;

class Pessoa extends TRecord
{
    const TABLENAME = 'pessoas';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
    
    // use SystemChangeLogTrait;
    

    /**
     * Constructor method
     */
    public function __construct($id = NULL)
    {
        parent::__construct($id);
        parent::addAttribute('nome');
        parent::addAttribute('cpf');
        parent::addAttribute('sexo');
        parent::addAttribute('cidade_id');
    }

    public function set_cidade(Cidade $cidade)
    {
        $this->cidade = $cidade;
        $this->cidade_id = $cidade->id;
    }
    
    public function get_cidade()
    {
        if (empty($this->cidade))
            $this->cidade = new Cidade($this->cidade_id);
        return $this->cidade;
    }
}
