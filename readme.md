Tarefa:

1 - Criar uma tabela de cidades no banco de dados contendo os campos id, nome, uf <- varchar (2) "RS", "SC"
2 - Criar uma listagem das cidades
3 - Criar um formulário das cidades -> o campo da uf precisa ser um Select  
4 - Entregar o CRUD (Create, Read, Update, Delete)

Como fazer a entrega:

1 - Criar branch com o que foi feito = git checkout -b "uma_descricao_sem_espacos_e_caracteres_especiais"
2 - Adicionar arquivos novos que foram criados = git add .
3 - Juntar o código (commitar) = git commit -am "descrição do que foi feito"
4 - Enviar código = git push. Se der uma mensagem de não ter o repósito, copiar o retorno e colar.
5 - Fazer o merge request, copiando o link que retorna após a criação.
6 - Gravar um vídeo (no loom ou no vidyard, demonstrando o que foi feito)

Obs: para voltar para a branch principal, utilize git checkout main

Links dos vídeos:

Banco de Dados - https://share.vidyard.com/watch/eFraqxdbFHYKAsJPSH4yEn?

Instalando framework - https://share.vidyard.com/watch/p1zcdXa3fMzdrQtjmCgf9F?

Navegação no sistema - https://share.vidyard.com/watch/eXp3RAAHdDhd4CK3AunNZs?

Problemas comuns (vai entender quando começar a mexer, pode pular e voltar depois se precisar) - https://share.vidyard.com/watch/hbkosK8X9s7C7LNDSKMaWE?

Construindo a listagem - https://share.vidyard.com/watch/Qy4f5pCdBMRQhxMLWbWYBu?

Cadastro - https://share.vidyard.com/watch/tjAZxQunTozsxcFj4DjE96?

Básico de Git - https://share.vidyard.com/watch/tUn9qdT5bepayPBgZMuHyK?

Primeira tarefa e entrega - https://share.vidyard.com/watch/2tXFZkqgcCzhn6FvYrSZ9P? e https://share.vidyard.com/watch/2ws138cApqgJwULJMZq6zw?

Relacionamentos - https://share.vidyard.com/watch/fBEcTYocvbFM2SbKQgqpXF?
