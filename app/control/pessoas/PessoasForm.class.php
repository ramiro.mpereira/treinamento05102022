<?php

use Adianti\Control\TAction;
use Adianti\Control\TPage;
use Adianti\Validator\TRequiredValidator;
use Adianti\Widget\Container\TVBox;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TRadioGroup;
use Adianti\Widget\Util\TXMLBreadCrumb;
use Adianti\Widget\Wrapper\TDBCombo;
use Adianti\Wrapper\BootstrapFormBuilder;

class PessoasForm extends TPage
{
    protected $form; 
    
    use Adianti\Base\AdiantiStandardFormTrait;
    
    function __construct()
    {
        parent::__construct();
        
        $this->setDatabase('treinamento');    // defines the database
        $this->setActiveRecord('Pessoa');   // defines the active record
        
        // creates the form
        $this->form = new BootstrapFormBuilder('form_Pessoas');
        $this->form->setFormTitle("Cadastro de Pessoa");
        $this->form->setClientValidation(true);
        
        // create the form fields
        $id       = new TEntry('id');
        $id->setEditable(FALSE);
       
        $nome = new TEntry('nome');
       
        $cpf = new TEntry('cpf');
        $cpf->setMask('999.999.999-99',true);

        $sexo     = new TRadioGroup('sexo');
        $opcoesSexo = ["M"=>'Masculino', "F" => "Feminino"];
        $sexo->addItems($opcoesSexo);
        
        $cidade_id = new TDBCombo('cidade_id','treinamento','Cidade','id','nome');
    
        // add the form fields
        $this->form->addFields( [new TLabel('Id:')], [$id] );
        $this->form->addFields( [new TLabel('Nome:','red')], [$nome] );
        $this->form->addFields( [new TLabel('CPF:')], [$cpf] );
        $this->form->addFields( [new TLabel('Sexo:','red')], [$sexo] );
        $this->form->addFields( [new TLabel('Cidade:')], [$cidade_id] );
        
        $nome->addValidation( 'Nome', new TRequiredValidator);
        $sexo->addValidation( 'Sexo', new TRequiredValidator);
        
        // define the form action
        $this->form->addAction('Salvar', new TAction(array($this, 'onSave')), 'fa:save green');
        $this->form->addActionLink('Limpar',  new TAction(array($this, 'onClear')), 'fa:eraser red');
        $this->form->addActionLink('Listagem',  new TAction(array('PessoasList', 'onReload')), 'fa:table blue');
        // wrap the page content using vertical box
        $vbox = new TVBox;
        $vbox->style = 'width: 100%';
      // $vbox->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $vbox->add($this->form);
        parent::add($vbox);
    }
}